provider "aws" {
  region = "ap-northeast-1"
  # access_key = ""
  # secret_key = ""
}
#Set the environment variables from terminal
# export AWS_SECRET_ACCESS_KEY=
# export AWS_ACCESS_KEY=


# variable "subnet_cidr_block" {
#   description = "subnet block for subnets"
#   #default     = "10.0.10.0/24"
#   type = list(string)
# }

# variable "cidr_blocks" {
#   description = "cidr blocks for vpc and subnets"
#   type = list(objects({
#     cidr_block = string
#     name       = string
#   }))
# }


variable "cidr_blocks" {
  description = "cidr blocks for vpc and subnets"
  type        = list(string)
}

# variable "vpc_cidr_block" {
#   description = "vpc cidr block"
# }

variable "environment" {
  description = "deployment environment"
}

resource "aws_vpc" "development-vpc" {
  cidr_block = var.cidr_blocks[0]
  tags = {
    "Name" = var.environment
  }

}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.development-vpc.id
  cidr_block        = var.cidr_blocks[1]
  availability_zone = "ap-northeast-1a"
  tags = {
    "Name" = "subnet-1-dev"
  }
}

data "aws_vpc" "existing_vpc" {
  default = true
}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id            = data.aws_vpc.existing_vpc.id
  cidr_block        = "172.31.48.0/20"
  availability_zone = "ap-northeast-1a"
  tags = {
    "Name" = "subnet-2-default"
  }
}

output "dev-vpn-id" {
  value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}

# output "dev-subnet-id-2" {
#   value = aws_subnet.dev-subnet-2.id
# }

